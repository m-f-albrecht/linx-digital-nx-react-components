import styled from 'styled-components';
import { margin } from 'styled-system';

const SectionContainer = styled.section<any>`
  display: flex;
  flex-direction: ${(props) => (props.row ? 'row' : 'column')};
  align-items: flex-start;
  justify-content: flex-start;
  ${margin};
`;

export { SectionContainer };
