import { IconLove } from '@linx-digital/nx-icons/react';
import { Button } from 'nx-components';
import React from 'react';

import { SectionContainer } from './container';

export default {
  title: 'Buttons/Info',
};

export const Info = () => {
  return (
    <SectionContainer row>
      <SectionContainer>
        <Button variant="info" mb={3}>
          AdvWorks
        </Button>
        <Button variant="info" iconLeft={<IconLove />} mb={3}>
          AdvWorks
        </Button>
        <Button variant="info" iconRight={<IconLove />} mb={3}>
          AdvWorks
        </Button>
        <Button variant="info" iconLeft={<IconLove />} mb={3} />
      </SectionContainer>

      <SectionContainer ml={3}>
        <Button variant="info" mb={3} pill>
          AdvWorks
        </Button>
        <Button variant="info" iconLeft={<IconLove />} mb={3} pill>
          AdvWorks
        </Button>
        <Button variant="info" iconRight={<IconLove />} mb={3} pill>
          AdvWorks
        </Button>
        <Button variant="info" iconLeft={<IconLove />} mb={3} pill />
      </SectionContainer>
    </SectionContainer>
  );
};
