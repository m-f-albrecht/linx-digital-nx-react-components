import { IconLove } from '@linx-digital/nx-icons/react';
import { Button } from 'nx-components';
import React from 'react';

import { SectionContainer } from './container';

export default {
  title: 'Buttons/Warning',
};

export const Warning = () => {
  return (
    <SectionContainer row>
      <SectionContainer>
        <Button variant="warning" mb={3}>
          AdvWorks
        </Button>
        <Button variant="warning" iconLeft={<IconLove />} mb={3}>
          AdvWorks
        </Button>
        <Button variant="warning" iconRight={<IconLove />} mb={3}>
          AdvWorks
        </Button>
        <Button variant="warning" iconLeft={<IconLove />} mb={3} />
      </SectionContainer>

      <SectionContainer ml={3}>
        <Button variant="warning" mb={3} pill>
          AdvWorks
        </Button>
        <Button variant="warning" iconLeft={<IconLove />} mb={3} pill>
          AdvWorks
        </Button>
        <Button variant="warning" iconRight={<IconLove />} mb={3} pill>
          AdvWorks
        </Button>
        <Button variant="warning" iconLeft={<IconLove />} mb={3} pill />
      </SectionContainer>
    </SectionContainer>
  );
};
