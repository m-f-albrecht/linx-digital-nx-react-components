import { IconLove } from '@linx-digital/nx-icons/react';
import { Button } from 'nx-components';
import React from 'react';

import { SectionContainer } from './container';

export default {
  title: 'Buttons/Danger',
};

export const Danger = () => {
  return (
    <SectionContainer row>
      <SectionContainer>
        <Button variant="danger" mb={3}>
          AdvWorks
        </Button>
        <Button variant="danger" iconLeft={<IconLove />} mb={3}>
          AdvWorks
        </Button>
        <Button variant="danger" iconRight={<IconLove />} mb={3}>
          AdvWorks
        </Button>
        <Button variant="danger" iconLeft={<IconLove />} mb={3} />
      </SectionContainer>

      <SectionContainer ml={3}>
        <Button variant="danger" mb={3} pill>
          AdvWorks
        </Button>
        <Button variant="danger" iconLeft={<IconLove />} mb={3} pill>
          AdvWorks
        </Button>
        <Button variant="danger" iconRight={<IconLove />} mb={3} pill>
          AdvWorks
        </Button>
        <Button variant="danger" iconLeft={<IconLove />} mb={3} pill />
      </SectionContainer>
    </SectionContainer>
  );
};
