import { CircularProgress } from 'nx-components';
import React from 'react';

export default {
  title: 'CircularProgress',
};

export const Default = () => {
  return (
    <>
      <CircularProgress
        color="color-context-negative-high"
        size={80}
        strokeWidth={10}
        percentage={35}
      />
      <CircularProgress />
    </>
  );
};
