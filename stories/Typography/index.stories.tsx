import { Typography } from 'nx-components';
import React from 'react';

export default {
  title: 'Typography',
};

export const Default = () => {
  return (
    <React.Fragment>
      <Typography
        element="h1"
        fontWeight="font-font-weight-font-weight-light"
        fontFamily="font-family-roboto"
        pl={5}
      >
        The quick brown fox jumps over the lazy dog
      </Typography>
      <Typography
        element="h2"
        lineHeight="font-line-height-line-height-superdistant"
        fontSize="font-font-size-font-size-giant"
      >
        The quick brown fox jumps over the lazy dog
      </Typography>
      <Typography
        element="h3"
        fontSize={[
          'font-font-size-font-size-md',
          'font-font-size-font-size-giant',
        ]}
      >
        The quick brown fox jumps over the lazy dog
      </Typography>
      <Typography
        element="h4"
        lineHeight="font-line-height-line-height-superdistant"
        fontSize="font-font-size-font-size-giant"
      >
        The quick brown fox jumps over the lazy dog
      </Typography>
      <Typography
        element="h5"
        lineHeight="font-line-height-line-height-superdistant"
        fontSize="font-font-size-font-size-giant"
      >
        The quick brown fox jumps over the lazy dog
      </Typography>
      <Typography
        element="h6"
        lineHeight="font-line-height-line-height-superdistant"
        fontSize="font-font-size-font-size-giant"
      >
        The quick brown fox jumps over the lazy dog
      </Typography>
      <Typography element="span">
        The quick brown fox jumps over the lazy dog
      </Typography>
      <Typography element="p">
        The quick brown fox jumps over the lazy dog
      </Typography>
      <Typography element="label">
        The quick brown fox jumps over the lazy dog
      </Typography>
    </React.Fragment>
  );
};
