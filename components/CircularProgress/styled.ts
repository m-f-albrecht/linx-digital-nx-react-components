import { ComponentType } from 'react';
import styled, { keyframes } from 'styled-components';

import { StyledCircularProgressProps } from './props';

const parseColor = (props: StyledCircularProgressProps) => {
  const { color } = props;
  const selectedColor = props.theme.colors[color];

  return `stroke: ${selectedColor};`;
};

export const parseToPx = (value: number) => {
  return `${value}px`;
};

const parseTransformOrigin = ({ semicircle }: StyledCircularProgressProps) => {
  return `transform-origin: ${parseToPx(semicircle)} ${parseToPx(semicircle)};`;
};

const RotateAnimation = keyframes`
  from {
    stroke-dashoffset: 0;
  } to {
    stroke-dashoffset: -220;
  }
`;

export const SVG: ComponentType<StyledCircularProgressProps> = styled.svg<StyledCircularProgressProps>`
  #circularProgressBackground {
    ${parseTransformOrigin};
    transform: rotate(-90deg);
  }

  #circularProgressCircle {
    animation: ${RotateAnimation} 1s linear infinite;
    ${parseColor};
  }
`;
