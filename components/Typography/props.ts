import {
  ColorTokens,
  FontSizeTokens,
  FontWeightTokens,
  LineHeightTokens,
} from '@linx-digital/nx-design-tokens/ts/tokens';
import { ReactNode } from 'react';
import { SpaceProps } from 'styled-system';

import { TypographyElements } from './types';

export interface TypographyProps extends SpaceProps {
  children: ReactNode;
  color?: ColorTokens;
  dataTestid?: string;
  element: TypographyElements;
  fontSize?: Array<FontSizeTokens> | FontSizeTokens;
  fontWeight?: FontWeightTokens;
  fontFamily?: string;
  lineHeight?: LineHeightTokens;
}

export interface StyledTypographyProps extends SpaceProps {
  children?: ReactNode;
  color?: Array<ColorTokens> | ColorTokens;
  fontFamily?: Array<string> | string;
  fontSize?: Array<FontSizeTokens> | FontSizeTokens;
  fontWeight?: Array<FontWeightTokens> | FontWeightTokens;
  lineHeight?: Array<LineHeightTokens> | LineHeightTokens;
}
