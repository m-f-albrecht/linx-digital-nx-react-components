import { ComponentType } from 'react';

import { StyledTypographyProps } from './props';

export type TypographyElements =
  | 'h1'
  | 'h2'
  | 'h3'
  | 'h4'
  | 'h5'
  | 'h6'
  | 'label'
  | 'p'
  | 'span';

export type TypographyElementsMapping = {
  [key in TypographyElements]?: ComponentType<StyledTypographyProps>;
};