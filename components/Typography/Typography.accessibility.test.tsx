import { render } from '@testing-library/react';
import { axe, toHaveNoViolations } from 'jest-axe';
import React from 'react';

import { LayoutProvider } from '../LayoutProvider';
import { Typography } from './index';

jest.useRealTimers();
expect.extend(toHaveNoViolations);

//accessibility test
describe('typography-accessibility', () => {
  it('should pass without errors', async () => {
    const { container } = render(
      <LayoutProvider layout="default">
        <Typography element="h1">teste</Typography>
      </LayoutProvider>,
    );

    const results = await axe(container);
    expect(results).toHaveNoViolations();
  });
});
