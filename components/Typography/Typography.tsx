import React, { ComponentType } from 'react';

import { TypographyProps, StyledTypographyProps } from './props';
import { H1, H2, H3, H4, H5, H6, Label, P, Span } from './styled';
import { TypographyElementsMapping } from './types';

const elementsMapping: TypographyElementsMapping = {
  h1: H1,
  h2: H2,
  h3: H3,
  h4: H4,
  h5: H5,
  h6: H6,
  label: Label,
  p: P,
  span: Span,
};

const Typography = ({
  children,
  color,
  dataTestid,
  element,
  fontSize,
  fontFamily,
  lineHeight,
  fontWeight,
  ...otherProps
}: TypographyProps) => {
  const Element: ComponentType<StyledTypographyProps> =
    elementsMapping[element] || Span;

  return (
    <Element
      color={color}
      data-testid={dataTestid}
      fontSize={fontSize}
      fontFamily={fontFamily}
      lineHeight={lineHeight}
      fontWeight={fontWeight}
      {...otherProps}
    >
      {children}
    </Element>
  );
};

Typography.defaultProps = {
  fontFamily: 'font-family-roboto',
};

export { Typography };
