import styled, { css } from 'styled-components';
import {
  color,
  fontSize,
  fontFamily,
  fontWeight,
  lineHeight,
  space,
} from 'styled-system';

import { StyledTypographyProps } from './props';

/**
 * NX Design System | Typography CSS Engine
 * @description
 */
const TypographyCSSEngine = css`
  ${color};
  ${fontSize};
  ${fontFamily};
  ${fontWeight};
  ${lineHeight};
  ${space};
`;

export const H1 = styled.h1<StyledTypographyProps>`
  ${TypographyCSSEngine}
`;
export const H2 = styled.h2<StyledTypographyProps>`
  ${TypographyCSSEngine}
`;
export const H3 = styled.h3<StyledTypographyProps>`
  ${TypographyCSSEngine}
`;
export const H4 = styled.h4<StyledTypographyProps>`
  ${TypographyCSSEngine}
`;
export const H5 = styled.h5<StyledTypographyProps>`
  ${TypographyCSSEngine}
`;
export const H6 = styled.h6<StyledTypographyProps>`
  ${TypographyCSSEngine}
`;
export const Label = styled.label<StyledTypographyProps>`
  ${TypographyCSSEngine}
`;
export const P = styled.p<StyledTypographyProps>`
  ${TypographyCSSEngine}
`;
export const Span = styled.span<StyledTypographyProps>`
  ${TypographyCSSEngine}
`;
