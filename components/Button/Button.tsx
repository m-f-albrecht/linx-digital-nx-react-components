import React from 'react';

import { Typography } from '../Typography';
import { ButtonProps, ButtonVariantMapping } from './props';
import { StyledButton } from './styled';

const Button = ({
  children,
  variant,
  iconLeft,
  iconRight,
  pill,
  ...otherProps
}: ButtonProps) => {
  const variantMapping: ButtonVariantMapping = {};

  variantMapping.CTA = {
    defaultColor: 'color-positive-regular',
    hoverColor: 'color-positive-light',
    textColor: 'color-neutral-lightest',
    layout: 'filled',
  };

  variantMapping.danger = {
    defaultColor: 'color-negative-regular',
    hoverColor: 'color-negative-light',
    textColor: 'color-neutral-lightest',
    layout: 'filled',
  };

  variantMapping.default = {
    activeColor: 'color-neutral-light',
    defaultColor: 'color-neutral-darkest',
    hoverColor: 'color-neutral-regular',
    textColor: 'color-neutral-lightest',
    layout: 'filled',
  };

  variantMapping.info = {
    defaultColor: 'color-info-regular',
    hoverColor: 'color-info-light',
    textColor: 'color-neutral-lightest',
    layout: 'filled',
  };

  variantMapping.line = {
    activeColor: 'color-neutral-light',
    defaultColor: 'color-neutral-darkest',
    hoverColor: 'color-neutral-regular',
    textColor: 'color-neutral-darkest',
    layout: 'outlined',
  };

  variantMapping.link = {
    activeColor: 'color-info-darkest',
    defaultColor: 'color-info-regular',
    hoverColor: 'color-info-light',
    textColor: 'color-info-regular',
    layout: 'ghost',
  };

  variantMapping.primary = {
    activeColor: 'color-linx-lightest',
    defaultColor: 'color-linx-regular',
    hoverColor: 'color-linx-light',
    textColor: 'color-neutral-lightest',
    layout: 'filled',
  };

  variantMapping.warning = {
    defaultColor: 'color-warning-regular',
    hoverColor: 'color-warning-light',
    textColor: 'color-neutral-lightest',
    layout: 'filled',
  };

  const mappedProps = variantMapping[variant];
  const radius = pill ? 'circle' : 'normal';

  return (
    <StyledButton
      iconLeft={iconLeft}
      iconRight={iconRight}
      borderRadius={radius}
      {...mappedProps}
      {...otherProps}
    >
      {iconLeft}
      {children && (
        <Typography
          element="span"
          fontSize="font-font-size-font-size-sm"
          fontWeight="font-font-weight-font-weight-medium"
          ml={iconLeft ? 1 : undefined}
          mr={iconRight ? 1 : undefined}
        >
          {children}
        </Typography>
      )}
      {iconRight}
    </StyledButton>
  );
};

Button.defaultProps = {
  pl: '16px',
  pr: '16px',
  variant: 'neutral',
};

export default Button;
