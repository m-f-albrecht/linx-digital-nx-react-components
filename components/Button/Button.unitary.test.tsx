import { IconLove } from '@linx-digital/nx-icons/react';
import { render } from '@testing-library/react';
import React from 'react';
import renderer from 'react-test-renderer';
import 'jest-styled-components';

import { LayoutProvider } from '../LayoutProvider';
import { Button } from './index';

describe('button component test suite', () => {
  it('button-defaultProps-variant', () => {
    const tree = renderer
      .create(
        <LayoutProvider layout="default">
          <Button variant="primary">button-defaultProps-variant</Button>
        </LayoutProvider>,
      )
      .toJSON();

    expect(tree).toHaveStyleRule(
      'background-color',
      'var(--color-linx-regular)',
    );
  });

  it('button-prop-icon', () => {
    const { getByTestId } = render(
      <LayoutProvider layout="default">
        <Button
          variant="primary"
          iconLeft={<IconLove data-testid="button-tests-icon" />}
        >
          button-prop-icon
        </Button>
      </LayoutProvider>,
    );

    const element = getByTestId('button-tests-icon');

    expect(element).toBeTruthy();
  });
});
