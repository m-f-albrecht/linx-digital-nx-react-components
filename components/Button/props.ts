import { ColorTokens } from '@linx-digital/nx-design-tokens/ts/tokens';
import { ButtonHTMLAttributes, ReactNode } from 'react';
import { BorderRadiusProps, MarginProps, PaddingProps } from 'styled-system';
import { ThemeProps } from 'theme/types';

import {
  ButtonLayoutTypes,
  ButtonSizeTypes,
  ButtonVariantTypes,
} from './types';

export interface ButtonProps
  extends ButtonHTMLAttributes<HTMLButtonElement>,
    MarginProps {
  children?: ReactNode;
  iconLeft?: ReactNode;
  iconRight?: ReactNode;
  size?: ButtonSizeTypes;
  pill?: boolean;
  theme?: ThemeProps;
  variant?: ButtonVariantTypes;
}
export interface StyledButtonLayoutProps {
  activeColor?: ColorTokens;
  defaultColor?: ColorTokens;
  hoverColor?: ColorTokens;
  textColor?: ColorTokens;
  layout?: ButtonLayoutTypes;
}
export interface StyledButtonProps
  extends ButtonHTMLAttributes<HTMLButtonElement>,
    BorderRadiusProps,
    MarginProps,
    PaddingProps,
    StyledButtonLayoutProps {
  children?: ReactNode;
  iconLeft?: ReactNode;
  iconRight?: ReactNode;
  pill?: boolean;
  theme?: ThemeProps;
}

export type ButtonVariantMapping = {
  [key in ButtonVariantTypes]?: StyledButtonProps;
};
