import { render } from '@testing-library/react';
import { axe, toHaveNoViolations } from 'jest-axe';
import React from 'react';

import { LayoutProvider } from '../LayoutProvider';
import { Skeleton } from './index';

jest.useRealTimers();
expect.extend(toHaveNoViolations);

//accessibility test
describe('typography-accessibility', () => {
  it('should pass without errors', async () => {
    const { container } = render(
      <LayoutProvider layout="default">
        <Skeleton width="100px" height="100px" animation="pulse" isCircle />
      </LayoutProvider>,
    );

    const results = await axe(container);
    expect(results).toHaveNoViolations();
  });
});
