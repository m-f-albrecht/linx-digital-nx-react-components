import { RadiusTokens } from '@linx-digital/nx-design-tokens/ts/tokens';
import { WidthProps, HeightProps } from 'styled-system';

import { AnimationType } from './types';

export interface SkeletonProps {
  animation?: AnimationType;
  borderRadius?: RadiusTokens;
  dataTestid?: string;
  height?: HeightProps & string;
  isCircle?: boolean;
  size?: string;
  width?: WidthProps & string;
}

export interface StyledSkeletonProps {
  animation?: AnimationType;
  isCircle?: boolean;
  size?: string;
}
