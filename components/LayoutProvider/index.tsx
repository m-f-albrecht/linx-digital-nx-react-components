import React, { cloneElement, FunctionComponent, ReactElement } from 'react';
import { ThemeProvider } from 'styled-components';

import { nxTheme } from '../../theme';

type Layouts = 'default'

type LayoutProviderProps = {
  layout: Layouts;
}

const LayoutProvider: FunctionComponent<LayoutProviderProps> = props => {

  const { children, ...otherProps } = props;

  const getTheme = () => {
    switch (props.layout) {
      case 'default':
      default:
        return nxTheme;
    }
  };

  return (
    <ThemeProvider theme={getTheme()}>
      {cloneElement(children as ReactElement, { ...otherProps })}
    </ThemeProvider>
  );
};

export { LayoutProvider };