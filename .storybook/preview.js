import { addDecorator } from '@storybook/react'
import { LayoutProvider } from '../components/LayoutProvider'
import '@linx-digital/nx-design-tokens/css/variables.css'

addDecorator(Story => (
  <LayoutProvider layout='default'>
    {Story()}
  </LayoutProvider>
))